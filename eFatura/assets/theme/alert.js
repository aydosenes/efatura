////Silme Kontrol İşlemleri
function alert(btnDelete) {
    if (btnDelete.dataset.confirmed) {
        // The action was already confirmed by the user, proceed with server event
        btnDelete.dataset.confirmed = false;
        return true;
    } else {
        event.preventDefault();
        swal({
            title: "Dikkat!",
            text: "Bu kaydı silmek istediğinize emin misiniz? ",
            icon: "warning",
            buttons: ["Vazgeç", "Evet, sil"],
            dangerMode: true
        }).then((isConfirm) => {
            if (isConfirm) {
                btnDelete.dataset.confirmed = true;
                btnDelete.click();
            }
        });
    }
}
////Silme Kontrol İşlemleri
//----------------------------------------------------------------------------------------------------------------------------------------------
////Genel Hata Mesajları
function hata() {
    swal("Hata!", "İşlem gerçekleşirken bir hata oluştu! Lütfen daha sonra tekrar deneyiniz.", "error")
}
////Genel Hata Mesajları
//----------------------------------------------------------------------------------------------------------------------------------------------
////Giriş ve Kaydol Sayfası
function girisuyari() {
    swal("Hata!", "Kullanıcı adı veya şifre hatalı! Lütfen bilgilerinizi kontrol ediniz..", "error")
}
function mailuyari() {
    swal("Dikkat!", "Girilen mail adresi zaten kayıtlı..", "warning")
}
function kayitbasarili() {
    swal("Harika", "Kayıt işleminiz başarıyla gerçekleşmiştir. 3 gün ile sınırlı olan ücretsiz deneme sürenizi uzatmak için iletişime geçebilirsiniz.", "success")
}
////Giriş ve Kaydol Sayfası
//----------------------------------------------------------------------------------------------------------------------------------------------
////Cariler Sayfası
function carisilinemez() {
    swal('Bu Cari Silinemez!', 'Bu cari stoklarda tedarikçi olarak kullanılıyor.', 'warning')
}
function carisilindi() {
    swal('Başarılı', 'Cari silme işlemi başarıyla gerçekleştirildi.', 'success')
}
function cariolusturuldu() {
    swal('Harika', 'Yeni cari hesabı başarıyla oluşturuldu.', 'success')
}
function carigrubueklendi() {
    swal('Harika', 'Yeni cari grubu başarıyla oluşturuldu.', 'success')
}
function carigrubukontrol() {
    swal('Dikkat!', 'Aynı isimde bir cari grubu zaten var.', 'warning')
}
function carigrubuguncelle() {
    swal('Başarılı', 'Cari grubu bilgileri başarıyla güncellendi.', 'success')
}
function varsayilancarigrubusilinemez() {
    swal('Hata!', 'Varsayılan cari grubu silinemez.', 'error')
}
function carigrubusil() {
    swal('Başarılı', 'Cari grubu başarıyla silindi.', 'success')
}
////Cariler Sayfası
//----------------------------------------------------------------------------------------------------------------------------------------------
////Stoklar Sayfası
function stoksilindi() {
    swal('Başarılı', 'Stok silme işlemi başarıyla gerçekleştirildi.', 'success')
}
////Stoklar Sayfası
//----------------------------------------------------------------------------------------------------------------------------------------------