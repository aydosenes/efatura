﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="yeni-fatura.aspx.cs" Inherits="hesaptutar.com__Yeni_.yeni_fatura" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>hesaptutar.com | Yeni Fatura</title>
    <script type="text/javascript">
        function openModal() {
            $('#urunListesi').modal('show');
        }
        function closeModal() {
            $('#urunListesi').modal('close');
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSiteMap" runat="server">
    <div class="breadcrumb-area">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/anasayfa">Genel Bakış</a></li>
                <li class="breadcrumb-item"><a href="/muhasebe">Muhasebe</a></li>
                <li class="breadcrumb-item active" aria-current="page">Yeni Faturad</li>
            </ol>
        </nav>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="content yeni-musteri" id="deneme" runat="server">
        <div class="container">
            <div class="row cnt">
                <div class="col-lg-12">
                    <span class="form-baslik mb-3">Fatura Bilgileri</span>
                    <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
                        <ContentTemplate>
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label" for="simpleinput">Fatura No</label>
                                        <div class="col-lg-8 mb-3">
                                            <asp:TextBox ID="txtFaturaNo" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label">Cari Tipi</label>
                                        <div class="col-lg-8 mb-3">
                                            <asp:DropDownList ID="ddlCariTipi" class="form-select" runat="server" AutoPostBack="true">
                                                <asp:ListItem>Kayıtlı Cari</asp:ListItem>
                                                <asp:ListItem>Yeni Cari</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label">Fatura Tipi</label>
                                        <div class="col-lg-8 mb-3">
                                            <asp:DropDownList ID="ddlFaturaTipi" AutoPostBack="true" class="form-select" runat="server">
                                                <asp:ListItem>Satış Faturası</asp:ListItem>
                                                <asp:ListItem>Alış Faturası</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label">Ödeme Şekli</label>
                                        <div class="col-lg-8 mb-3">
                                            <asp:DropDownList ID="ddlOdemeSekli" class="form-select" runat="server">
                                                <asp:ListItem>Nakit</asp:ListItem>
                                                <asp:ListItem>Çek</asp:ListItem>
                                                <asp:ListItem>Açık Hesap</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label">İl</label>
                                        <div class="col-lg-8 mb-3">
                                            <asp:DropDownList ID="ddlIl" class="js-example-basic-single form-select" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label" for="simpleinput">Fatura Tarihi</label>
                                        <div class="col-lg-8 mb-3">
                                            <asp:TextBox ID="txtFaturaTarihi" type="datetime-local" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label" for="simpleinput">Vade Tarihi</label>
                                        <div class="col-lg-8 mb-3">
                                            <asp:TextBox ID="txtVadeTarihi" type="date" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label">Cari Ünvanı</label>
                                        <div class="col-lg-8 mb-3">
                                            <asp:DropDownList ID="ddlCariler" AutoPostBack="true" class="js-example-basic-single form-select" runat="server"></asp:DropDownList>
                                            <asp:TextBox ID="txtCariUnvan" Visible="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label">Para Birimi</label>
                                        <div class="col-lg-8 input-group mb-3">
                                            <asp:DropDownList ID="ddlParaBirimi" class="form-select" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlParaBirimi_SelectedIndexChanged">
                                                <asp:ListItem>TRY</asp:ListItem>
                                                <asp:ListItem>USD</asp:ListItem>
                                                <asp:ListItem>EUR</asp:ListItem>
                                                <asp:ListItem>GBP</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:TextBox ID="txtKur" Visible="false" ReadOnly CssClass="form-control" runat="server" Style="width: 60px" AutoPostBack="true"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label">İlçe</label>
                                        <div class="col-lg-8 mb-3">
                                            <asp:DropDownList ID="ddlIlce" class="js-example-basic-single form-select" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label" for="simpleinput">Vergi Dairesi</label>
                                        <div class="col-lg-8 mb-3">
                                            <asp:TextBox ID="txtVergiDairesi" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label" for="simpleinput">Vergi Numarası</label>
                                        <div class="col-lg-8 mb-3">
                                            <asp:TextBox ID="txtVergiNo" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label" for="simpleinput">Açıklama</label>
                                        <div class="col-lg-8 mb-3">
                                            <asp:TextBox ID="txtAciklama" TextMode="MultiLine" Style="height: 90px; resize: none;" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label" for="simpleinput">Adres</label>
                                        <div class="col-lg-8 mb-3">
                                            <asp:TextBox ID="txtAdres" TextMode="MultiLine" Rows="1" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <asp:Label ID="lblUSD" Visible="true" runat="server" Text="Label"></asp:Label>
                            <asp:Label ID="lblEUR" Visible="false" runat="server" Text="Label"></asp:Label>
                            <asp:Label ID="lblGBP" Visible="false" runat="server" Text="Label"></asp:Label>
                            <hr>
                            <asp:Label ID="lblDurum" runat="server" Visible="false" Text="normal"></asp:Label>
                            <div class="row contact-person">
                                <asp:GridView ID="dgwUrunler" runat="server" CssClass="table table-sm mb-0 no-plc fatura-urunler" BorderStyle="None" AutoGenerateColumns="False" OnRowDataBound="dgwUrunler_RowDataBound">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-CssClass="byk" HeaderText="Ürün / Hizmet">
                                            <ItemTemplate>
                                                <div class="input-group">
                                                    <asp:TextBox ID="txtUrunAdi" CssClass="form-control" Text='<%# Eval("UrunAdi") %>' runat="server"></asp:TextBox>
                                                    <asp:LinkButton ID="LinkButton2" runat="server" CssClass="btn btn-outline-secondary"><i class="fas fa-search"></i></asp:LinkButton>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Miktar">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtMiktar" AutoPostBack="true" AutoCompleteType="Disabled" CssClass="form-control" Text='<%# Eval("Miktar") %>' onkeypress="return SayiGirme(event)" runat="server" OnTextChanged="OnTextChanged"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Birim">
                                            <ItemTemplate>
                                                <asp:DropDownList ID="ddlBirim" CssClass="form-select" runat="server">
                                                    <asp:ListItem>Adet</asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Birim Fiyatı">
                                            <ItemTemplate>
                                                <div class="input-group">
                                                    <asp:TextBox ID="txtBirimFiyat" CssClass="form-control otvb" Style="padding-right: 0" AutoCompleteType="Disabled" Text='<%# Eval("BirimFiyat") %>' onkeypress="return SayiGirme(event)" runat="server" AutoPostBack="true" OnTextChanged="OnTextChanged"></asp:TextBox>
                                                    <asp:Label ID="lblParaBirimi" CssClass="form-control" runat="server" Text='<%# Eval("ParaBirimi") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="KDV">
                                            <ItemTemplate>
                                                <asp:DropDownList ID="ddlKDV" CssClass="form-select" runat="server" DataTextField='<%# Eval("KDV") %>' AutoPostBack="true" OnSelectedIndexChanged="SelectedIndexChanged">
                                                    <asp:ListItem Text="%18" Value="18"></asp:ListItem>
                                                    <asp:ListItem Text="%8" Value="8"></asp:ListItem>
                                                    <asp:ListItem Text="%1" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="%0" Value="0"></asp:ListItem>
                                                </asp:DropDownList><br />
                                                <asp:Label ID="lblotv" Visible="false" runat="server" Text='<%# Eval("otvDurum") %>'></asp:Label>
                                                <asp:Label ID="lbloiv" Visible="false" runat="server" Text='<%# Eval("oivDurum") %>'></asp:Label>
                                                <div class="otv-kapsa">
                                                    <div class="input-group" style="margin-top: 5px;">
                                                        <asp:TextBox ID="txtOTV" placeholder="ÖTV" AutoPostBack="true" CssClass="form-control otvb" Text='<%# Eval("OTV") %>' runat="server" OnTextChanged="otvHesap"></asp:TextBox>
                                                        <asp:DropDownList ID="ddlotvGirisTuru" AutoPostBack="true" CssClass="form-select otvk" runat="server" OnSelectedIndexChanged="ddlotvGirisTuru_SelectedIndexChanged">
                                                            <asp:ListItem Value="1">%</asp:ListItem>
                                                            <asp:ListItem Value="2">₺</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    <asp:LinkButton ID="lbOtvKaldir" runat="server" OnClick="lbOtvKaldir_Click" class="otv-kapat"><i class="fas fa-times"></i></asp:LinkButton>
                                                </div>
                                                <div class="otv-kapsa">
                                                    <div class="input-group" style="margin-top: 5px;">
                                                        <asp:TextBox ID="txtOIV" placeholder="ÖIV" AutoPostBack="true" CssClass="form-control otvb" Text='<%# Eval("OIV") %>' runat="server" OnTextChanged="oivHesap"></asp:TextBox>
                                                        <asp:Label ID="yuzde" CssClass="form-control otvk" Style="padding-top: 7px" runat="server" Text="%"></asp:Label>
                                                    </div>
                                                    <asp:LinkButton ID="lbOivKaldir" runat="server" OnClick="lbOivKaldir_Click" class="otv-kapat"><i class="fas fa-times"></i></asp:LinkButton>
                                                </div>

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Toplam">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtToplam" CssClass="form-control" AutoCompleteType="Disabled" Text='<%# Eval("Toplam") %>' runat="server" onkeypress="return SayiGirme(event)" AutoPostBack="true" OnTextChanged="OnTextChanged"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-CssClass="sil-baslik" HeaderText="">
                                            <ItemTemplate>
                                                <button type="button" class="dropdown-toggle dropdown-toggle-split vergi-ekle" style="padding: 0;" data-bs-toggle="dropdown" aria-expanded="false">
                                                    <span class="visually-hidden">Toggle Dropdown</span>
                                                    <span style="font-size: 32px; margin-top: 3px; display: block;"><i class="fas fa-coins"></i></span>
                                                </button>
                                                <ul class="dropdown-menu">
                                                    <li>
                                                        <asp:LinkButton ID="lbOTVEkle" runat="server" OnClick="lbOTVEkle_Click">ÖTV Ekle</asp:LinkButton>
                                                    </li>
                                                    <li>
                                                        <asp:LinkButton ID="lbOIVEkle" runat="server" OnClick="lbOIVEkle_Click">ÖİV Ekle</asp:LinkButton>
                                                    </li>
                                                </ul>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-CssClass="sil-baslik" HeaderText="">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="LinkButton1" runat="server" OnClick="cmdDel_Click"><span class="satir-sil"><i class="fas fa-times"></i></span></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>

                            <div class="input-group mt-3">
                                <asp:LinkButton ID="lbSatirEkle" CssClass="btn btn-danger" runat="server" OnClick="lbSatirEkle_Click"><span><i class="fas fa-plus"></i></span> Satır Ekle</asp:LinkButton>

                            </div>


                            <div class="row justify-content-end">
                                <div class="fatura-tutarlar">

                                    <table class="fatura-table">
                                        <tbody>
                                            <tr>
                                                <th>Mal/Hizmet Toplam Tutarı</th>
                                                <td class="price">
                                                    <asp:Label ID="lblMalHizmetTutar" CssClass="form-control" BorderStyle="None" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <asp:Panel ID="pnlHesaplananOtv" Visible="false" runat="server">
                                                    <th>
                                                        <asp:Label ID="lblTpOtv" Text="Hesaplanan ÖTV" runat="server"></asp:Label>
                                                    </th>
                                                    <td class="price">
                                                        <asp:Label ID="lblHesaplananOtv" CssClass="form-control" BorderStyle="None" Text="0,00" runat="server"></asp:Label>
                                                    </td>
                                                </asp:Panel>
                                            </tr>
                                            <tr>
                                                <asp:Panel ID="pnlHesaplananOiv" Visible="false" runat="server">
                                                    <th>
                                                        <asp:Label ID="lblTpOiv" Text="Hesaplanan ÖİV" runat="server"></asp:Label>
                                                    </th>
                                                    <td class="price">
                                                        <asp:Label ID="lblHesaplananOiv" CssClass="form-control" BorderStyle="None" Text="0,00" runat="server"></asp:Label>
                                                    </td>
                                                </asp:Panel>
                                            </tr>
                                            <tr>
                                                <th>Hesaplanan KDV</th>
                                                <td class="price">
                                                    <asp:Label ID="lblHesaplananKDV" CssClass="form-control" BorderStyle="None" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Vergiler Dahil Toplam Tutar</th>
                                                <td class="price">
                                                    <asp:Label ID="lblVergilerDahilTutar" CssClass="form-control" BorderStyle="None" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>İskonto Tutarı</th>
                                                <td class="price">
                                                    <asp:TextBox ID="txtIskontoTutari" AutoPostBack="true" CssClass="form-control" onkeypress="return SayiGirme(event)" runat="server" OnTextChanged="txtIskontoTutari_TextChanged"></asp:TextBox>

                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Ödenecek Tutar</th>
                                                <td class="price">
                                                    <asp:Label ID="lblOdenecekTutar" CssClass="form-control" BorderStyle="None" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    <asp:Label ID="lblTahsil" runat="server" Text="Tahsil Edilen"></asp:Label>
                                                </th>
                                                <td class="price">
                                                    <asp:TextBox ID="txtTahsilEdilen" AutoPostBack="true" CssClass="form-control" onkeypress="return SayiGirme(event)" runat="server" OnTextChanged="txtTahsilEdilen_TextChanged"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="d-grid gap-2 d-md-flex mt-5 justify-content-end">
                                <div>
                                    <button class="btn btn-primary me-md-2" type="button" onclick="javascript:history.go(-1)">Vazgeç</button>
                                    <div class="btn-group">
                                        <asp:Button ID="btnKaydet" CssClass="btn btn-danger" runat="server" Text="Kaydet" ValidationGroup="faturabil" />

                                        <button type="button" class="btn btn-danger dropdown-toggle dropdown-toggle-split" data-bs-toggle="dropdown" aria-expanded="false">
                                            <span class="visually-hidden">Toggle Dropdown</span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <asp:LinkButton ID="lbKaydetveYeni" CssClass="dropdown-item" runat="server" ValidationGroup="faturabil">Kaydet ve Yeni Ekle</asp:LinkButton>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="lbSatirEkle" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>

        </div>
    </div>

    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <div class="popup" runat="server" id="popUp" visible="false">
                <span>
                    <i class="far fa-thumbs-up" runat="server" id="basarili"></i>
                    <i class="far fa-thumbs-down" runat="server" id="basarisiz"></i>
                    <i class="fas fa-exclamation-triangle" runat="server" id="uyari"></i>
                    <asp:Label ID="lblMesaj" runat="server" Text="Label"></asp:Label></span>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    
    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
            <div class="popup show" runat="server" style="background: #48CAE4" id="loading">
                <span>
                    <i class="fas fa-cog fa-spin"></i>
                    <asp:Label ID="Label1" runat="server" Text="İşleminiz gerçekleştiriliyor..."></asp:Label></span>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <script>
        function mesaj() {
            $(document).ready(function () {
                setTimeout(function () {
                    $(".popup").fadeOut("slow", function () {
                        $(".popup").remove();
                    });
                }, 3000);
            });
        }
    </script>
    <!-- Datatables -->
    <script>
        $.extend($.fn.dataTable.defaults, {
            responsive: true
        });
        $(document).ready(function () {
            $('#urunList').DataTable({
                dom: 'lfrtip',
                "pageLength": 25,
            });
        });
    </script>
    <script>
        function pageLoad() {
            $('.js-example-basic-single').select2();
        }
    </script>
</asp:Content>
